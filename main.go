package main

import (
	"./cmd/auth-api"
	"log"
)

func main() {

	err :=auth_api.CreateAuthHandler(auth_api.CreateApp(":5000"))

	if err!=nil{
		log.Fatal(err)
	}
}
