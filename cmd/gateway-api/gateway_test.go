package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"strings"
	"log"
)
type RegUserCase struct {
	Input  []byte
	Output error
	OutputStatus
}
var RegUserTestCases = []RegUserCase{
	{
		Input: []byte(`{
			"first_name": "Павел",
				"last_name": "Дуров",
				"birthday":"1905-10-10",
				"email": "durov@telegram7.org",
				"password": "qwerty"
		}`),
		Output: nil,
	},
	{
		Input: []byte(`{
			"first_name": "Павел",
				"last_name": "Дуров",
				"birthday":"1905-10-10",
				 
				"password": "qwerty"
		}`),
		Output: errors.ErrBadReq,
	},
	{
		Input: []byte(`{
			"first_name": "",
				"last_name": "Дуров",
				"birthday":"1905-10-10",
				"email": "durov@telegram7.org",
				"password": "qwerty"
		}`),
		Output: errors.ErrBadReq,
	},
	{
		Input: []byte(`{
			"first_name": "Дуров",
				"last_name": "",
				"birthday":"1905-10-10",
				"email": "durov@telegram7.org",
				"password": "qwerty"
		}`),
		Output: errors.ErrBadReq,
	}, {
		Input: []byte(`{
			"first_name": "Дуров",
				"last_name": "Дуров",
				"birthday":"",
				"email": "durov@telegram7.org",
				"password": "qwerty"
		}`),
		Output: nil,
	}, {
		Input: []byte(`{
			"first_name": "Дуров",
				"last_name": "Дуров",
				"birthday":"1995-10-10",
				"email": "",
				"password": "qwerty"
		}`),
		Output: errors.ErrBadReq,
	},
	{
		Input: []byte(`{
			"first_name": "Дуров",
				"last_name": "Дуров",
				"birthday":"1995-10-10",
				"email": " durov@telegram7.org",
				"password": ""
		}`),
		Output: errors.ErrBadReq,
	},
}


func TestRegHandler(t *testing.T) {
	DSN := "user=postgres password=1234 dbname=test sslmode=disable"
	a, err := CreateGateWayApp(":5000", DSN, DSN)
	if err != nil {
		log.Fatal("can`t run gateway app")
	}
	DUMMY := strings.NewReader(`{
 "first_name": "Павел",
 "last_name": "Дуров",
	"birthday":"1905-10-10",
 "email": "durov@telegram7.org",
 "password": "qwerty"
}`)

	req, err := http.NewRequest(http.MethodPost, "/signup", DUMMY)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(a.RegHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusConflict {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `{"error":"email already exists"}`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
